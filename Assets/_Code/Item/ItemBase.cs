using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBase : MonoBehaviour
{
    public bool isActive = true;

    void Start()
    {

    }

    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            if (!isActive)
                return;

            isActive = false;
            collision.GetComponent<Player>().AddItem(this);
        }
    }



    public void Removed()
    {
        Destroy(gameObject, 1f);
    }
}
