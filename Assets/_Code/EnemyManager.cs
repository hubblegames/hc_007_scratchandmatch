using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class EnemyManager : MonoBehaviour
{

    #region EnemyManager |------------------------
    private static EnemyManager _ins;

    public static EnemyManager Ins
    {
        get
        {
            if (_ins == null)
                _ins = FindObjectOfType<EnemyManager>();
            return _ins;
        }
    }
    #endregion

    public List<Enemy> Enemies = new List<Enemy>();

    public Enemy GetClosestOne()
    {
        return Enemies.OrderBy(e => (e.transform.position - Player.Ins.transform.position).sqrMagnitude).First();
    }
}
