using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using DG.Tweening;

public class Enemy : MonoBehaviour
{
    [Header("Values")]
    public float lookDistance = 3;
    public bool isMoving = false;

    [Header("Components")]
    public Rigidbody2D compRigidbody;
    public Animator compAnimator;

    [Header("Prefabs")]
    public GameObject prdExplode;

    private void Start()
    {
        compRigidbody = GetComponent<Rigidbody2D>();
        compAnimator = GetComponentInChildren<Animator>();
    }

    private void Update()
    {
        LookForPlayer();
    }

    public void LookForPlayer()
    {
        if (Vector3.Distance(Player.Ins.transform.position, transform.position) <= lookDistance)
        {
            compAnimator.SetTrigger("Run");
            Vector3 direction = Player.Ins.transform.position - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0, 0, angle - 90);
            if (!isMoving)
            {
                isMoving = true;
                transform.DOMove(Player.Ins.transform.position, 3f).SetEase(Ease.Linear);
            }
        }
    }

    private void OnEnable()
    {
        EnemyManager.Ins.Enemies.Add(this);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Player player = collision.GetComponent<Player>();
            player.GetDamage();

            //TODO Particle
            EnemyManager.Ins.Enemies.Remove(this);
            Destroy(gameObject);
        }
    }

    public void Dead()
    {
        //Todo Particle
        EnemyManager.Ins.Enemies.Remove(this);
        Vector3 pos = new Vector3(transform.position.x, transform.position.y, transform.position.z - 2);
        Instantiate(prdExplode, pos, Quaternion.identity);
        Destroy(gameObject);
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.color = Color.red;
        Handles.DrawWireDisc(transform.position, Vector3.forward, lookDistance);
    }
#endif
}
