using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Bullet : MonoBehaviour
{
    public float speed = 2;

    void Start()
    {
        DestroyTimer();
    }

    void Update()
    {
        transform.Translate(transform.forward * speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Enemy"))
        {
            collision.GetComponent<Enemy>().Dead();
            Destroy(gameObject);
        }
    }

    public void SetMovePosition(Enemy enemy)
    {
        Vector3 positionMove = new Vector3(enemy.transform.position.x, enemy.transform.position.y, enemy.transform.position.z - 2);
        transform.DOMove(positionMove, 0.5f);
    }

    public void DestroyTimer()
    {
        DOTween.Sequence().
            AppendInterval(4f)
            .AppendCallback(() => Destroy(gameObject));
    }


}
