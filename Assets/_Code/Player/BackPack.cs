using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;

public class BackPack : MonoBehaviour
{
    [Header("Values")]
    public List<ItemBase> Items = new List<ItemBase>();

    [Header("Components")]
    public Transform compBackPackPosition;
    public Player compPlayer;
    public GameObject prfStar;

    void Start()
    {
        compPlayer = GetComponentInParent<Player>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
            RemoveLastThree();
    }

    public void RemoveItem(ItemBase item)
    {
        Items.Remove(item);
        item.transform.DOLocalJump(GetRandomPosition(), 2, 2, 0.3f);
        item.transform.DOScale(Vector3.zero, 1f).OnComplete(() => Destroy(item.gameObject));
    }

    public void AddItem(ItemBase item)
    {
        Items.Add(item);
        item.transform.SetParent(compBackPackPosition);
        item.transform.DOLocalJump(GetPosition(), 1, 1, 0.3f);
        item.transform.DORotate(Vector3.zero, 0.3f);
    }

    public void RemoveLastThree()
    {
        for (int i = 0; i < 3; i++)
            Upgrade(GetLast());

        Player.Ins.UpgradeGun();
    }

    public void Upgrade(ItemBase item)
    {
        Items.Remove(item);
        item.transform.DOLocalJump(Vector3.zero, 3, 1, 0.5f);
        Instantiate(prfStar,  compPlayer.compAttackPos.position, Quaternion.identity);
        item.transform.DOScale(Vector3.zero, 1f)
            .OnComplete(() =>
            {
                compPlayer.compGun.transform.DOPunchScale(compPlayer.compGun.transform.localScale, 0.3f, 1);
                Destroy(item.gameObject);
            });
        Player.Ins.health -= 1;
    }

    public ItemBase GetLast()
    {
        return Items.Last();
    }

    public Vector3 GetPosition()
    {
        return new Vector3(0, Items.Count, 0);
    }

    public Vector3 GetRandomPosition()
    {
        Vector2 rand = Random.insideUnitCircle * 3;
        return new Vector3(rand.x, 0, rand.y);
    }
}
