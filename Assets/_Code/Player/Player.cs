using StarterPack._Code.MainLevel;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using DG.Tweening;

public class Player : MonoBehaviour, ILevelCanControl
{

    #region Player |------------------------
    private static Player _ins;

    public static Player Ins
    {
        get
        {
            if (_ins == null)
                _ins = FindObjectOfType<Player>();
            return _ins;
        }
    }
    #endregion

    [Header("Values")]
    public bool isActive = false;
    public float speed = 5f;
    public int health = 1;
    public float attackRange = 7f;

    private float timeWhenAllowedNextShoot = 0f;
    public float timeBetweenShooting = 1f;

    [Header("Components")]
    public Rigidbody2D compRigidbody;
    public Animator compAnimator;
    public BackPack compBackPack;
    public Transform compAttackPos;
    public Transform compGun;

    [Header("Prefabs")]
    public GameObject prfBullet;
 
    void Start()
    {
        compRigidbody = GetComponent<Rigidbody2D>();
        compAnimator = GetComponentInChildren<Animator>();
        compRigidbody.bodyType = RigidbodyType2D.Kinematic;
    }

    void Update()
    {
        if (!isActive)
            return;

        compRigidbody.velocity = new Vector3(0, speed, 0);

        FindClosest();
    }

    public void FindClosest()
    {
        Enemy enemy = EnemyManager.Ins.GetClosestOne();

        if (Vector3.Distance(enemy.transform.position, transform.position) <= attackRange)
        {
            Vector3 direction = enemy.transform.position - transform.position;
            float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
            transform.DORotate(new Vector3(0, 0, angle - 90), 0.1f);
            CheckIfShouldShoot(enemy);
            compAnimator.SetLayerWeight(1, 1);
        }
        else
        {
            compAnimator.SetLayerWeight(1, 0);
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }

    public void CheckIfShouldShoot(Enemy enemy)
    {
        if (timeWhenAllowedNextShoot <= Time.time)
        {
            timeWhenAllowedNextShoot = Time.time + timeBetweenShooting;
            Attack(enemy);
        }
    }

    public void Attack(Enemy enemy)
    {
        Bullet bullet = Instantiate(prfBullet, compAttackPos.position, Quaternion.identity).GetComponent<Bullet>();
        bullet.SetMovePosition(enemy);
    }

    public void UpgradeGun()
    {
        timeBetweenShooting -= 0.1f;
    }

    public void GetDamage()
    {
        if (health - 1 <= 0)
        {
            isActive = false;
            compRigidbody.velocity = Vector3.zero;
            compRigidbody.bodyType = RigidbodyType2D.Kinematic;
            LevelManager.Ins.OnLose();
        }
        else
        {
            RemoveItem();
            health -= 1;
        }
    }

    public void RemoveItem()
    {
        compBackPack.RemoveItem(compBackPack.GetLast());
    }

    public void AddItem(ItemBase item)
    {
        compBackPack.AddItem(item);
        health++;
    }

    public void OnStart()
    {
        compRigidbody.bodyType = RigidbodyType2D.Dynamic;
        isActive = true;
        compAnimator.SetTrigger("Run");
    }

    public void OnWin()
    {
        compRigidbody.velocity = Vector3.zero;
        isActive = false;
        compRigidbody.bodyType = RigidbodyType2D.Kinematic;
    }

    public void OnLose()
    {
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.color = Color.blue;
        Handles.DrawWireDisc(transform.position, Vector3.forward, attackRange);
    }
#endif
}
