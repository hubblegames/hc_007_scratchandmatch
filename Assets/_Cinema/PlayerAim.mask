%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: PlayerAim
  m_Mask: 00000000010000000100000000000000000000000100000001000000010000000100000000000000000000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Basketball_player_blonde
    m_Weight: 1
  - m_Path: metarig
    m_Weight: 1
  - m_Path: metarig/Pelvis
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/Neck
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/Neck/Head
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/shoulder.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/shoulder.L/upper_arm.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/shoulder.L/upper_arm.L/forearm.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/shoulder.L/upper_arm.L/forearm.L/hand.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/shoulder.L/upper_arm.L/forearm.L/hand.L/hand.L.f1
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/shoulder.L/upper_arm.L/forearm.L/hand.L/hand.L.f1/hand.L.f2
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/shoulder.L/upper_arm.L/forearm.L/hand.L/hand.L.f1/hand.L.f2/hand.L.f3
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/shoulder.L/upper_arm.L/forearm.L/hand.L/hand.L.thumb.1
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/shoulder.L/upper_arm.L/forearm.L/hand.L/hand.L.thumb.1/hand.L.thumb.2
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/shoulder.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/shoulder.R/upper_arm.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/shoulder.R/upper_arm.R/forearm.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/shoulder.R/upper_arm.R/forearm.R/hand.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/shoulder.R/upper_arm.R/forearm.R/hand.R/hand.R.f1
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/shoulder.R/upper_arm.R/forearm.R/hand.R/hand.R.f1/hand.R.f2
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/shoulder.R/upper_arm.R/forearm.R/hand.R/hand.R.f1/hand.R.f2/hand.R.f3
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/shoulder.R/upper_arm.R/forearm.R/hand.R/hand.R.thumb.1
    m_Weight: 1
  - m_Path: metarig/Pelvis/spine.1/spine.2/shoulder.R/upper_arm.R/forearm.R/hand.R/hand.R.thumb.1/hand.R.thumb.2
    m_Weight: 1
  - m_Path: metarig/Pelvis/thigh.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/thigh.L/shin.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/thigh.L/shin.L/foot.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/thigh.L/shin.L/foot.L/toe.L
    m_Weight: 1
  - m_Path: metarig/Pelvis/thigh.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/thigh.R/shin.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/thigh.R/shin.R/foot.R
    m_Weight: 1
  - m_Path: metarig/Pelvis/thigh.R/shin.R/foot.R/toe.R
    m_Weight: 1
