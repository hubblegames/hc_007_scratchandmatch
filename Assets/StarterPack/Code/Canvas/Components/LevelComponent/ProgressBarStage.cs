﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UI;

namespace Plugins._StarterPack.Code.Canvas.Components.LevelComponent
{
    [ExecuteAlways]
    public class ProgressBarStage : ProgressBar
    {
        [Header("Stage Settings")] public GameObject prefStageActivate;
        public GameObject prefStageDeactivate;
        public List<StageObject> stageObjectList;

        // Update is called once per frame
        [ContextMenu("Set Stages")]
        private void SetStages()
        {
            Width = compSlider.GetComponent<RectTransform>().rect.width;
            OffsetX = compCursor.rect.width / 2f;

            foreach (var stage in stageObjectList)
            {
                var newStageDeactivate = Instantiate(prefStageDeactivate, Vector3.zero, Quaternion.identity,
                    transform.GetChild(0));
                newStageDeactivate.GetComponent<RectTransform>().anchoredPosition =
                    Vector2.right * (Width * stage.rate);

                var newStageActivate = Instantiate(prefStageActivate, Vector3.zero, Quaternion.identity,
                    compSlider.transform);
                newStageActivate.GetComponent<RectTransform>().anchoredPosition =
                    Vector2.right * (Width * stage.rate);
            }
        }
    }

    [Serializable]
    public class StageObject
    {
        public float rate;
        public Image icon;
    }
}