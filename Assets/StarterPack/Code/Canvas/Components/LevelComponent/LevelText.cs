﻿using TMPro;
using UnityEngine;

namespace StarterPack._Code.Canvas.Components.LevelComponent
{
    public class LevelText : MonoBehaviour
    {
        [Header("Component")] public TextMeshProUGUI compCurLevel;
        public string prefix = "Level ";
        public int levelOffset;

        public void SetLevel(int level)
        {
            level += levelOffset;
            compCurLevel.text = prefix + level;
        }
    }
}