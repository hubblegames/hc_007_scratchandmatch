﻿using System;
using System.Collections.Generic;
using Code.Scripts.Manager.Canvas.Enum;
using DG.Tweening;
using Plugins._StarterPack.Code.Canvas.Base.Panels;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace Plugins._StarterPack.Code.Canvas.CustomPanels
{
    public class MatchMakingPanel : MonoBehaviour
    {
        public Slider compMatchMakingSlider;

        public TextMeshProUGUI compBotName;
        public List<Transform> compEnemies = new List<Transform>();

        private static readonly List<string> TeamNames = new List<string>()
        {
            "Ari", "Editha", "Wow"
        };

        private FixedPanel _fixedPanel;

        private void Start()
        {
            _fixedPanel = GetComponent<FixedPanel>();

            _fixedPanel.onOpenEvents.AddListener(OnOpen);
            _fixedPanel.onCloseEvents.AddListener(OnClose);
        }

        private static void OnOpen()
        {
        }

        private static void OnClose()
        {
        }

        public void UpdateSliderValue()
        {
            const int duration = 5;
            var randomRate = Random.Range(0.3f, 0.6f);

            var firstDuration = duration * randomRate;
            var secondDuration = duration * (1 - randomRate);

            var randomFill = Random.Range(0.3f, 0.5f);

            DOTween.Sequence()
                // --- Search for Enemy Team
                .Append(compMatchMakingSlider.DOValue(randomFill, firstDuration))
                .AppendCallback(() =>
                {
                    DisplayEnemyTeam();
                    compBotName.text = SelectEnemyTeamName();
                })
                // --- Load Game
                .Append(compMatchMakingSlider.DOValue(1, secondDuration))
                .AppendCallback(() =>
                {
                    _fixedPanel.OpenPanelById(CanvasId.StartGame);
                    _fixedPanel.CloseYourSelf();
                });
        }

        private void DisplayEnemyTeam()
        {
            foreach (var enemies in compEnemies)
            {
                DOTween.Sequence()
                    .Append(enemies.DOScale(1, 1f))
                    .Append(enemies.DOShakeScale(1, 1f));
            }
        }

        private static string SelectEnemyTeamName()
        {
            var rand = Random.Range(0, TeamNames.Count);
            return TeamNames[rand].ToString();
        }
    }
}