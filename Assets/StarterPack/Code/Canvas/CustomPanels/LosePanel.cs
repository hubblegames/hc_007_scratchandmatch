﻿using StarterPack._Code.MainLevel;
using UnityEngine;

namespace _Code.StarterPack.Canvas.CustomPanels
{
    public class LosePanel : MonoBehaviour
    {
 
        public void BTN_Restart()
        {
            LevelManager.OnRestartLevel();
        }
        
    }
}