﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Plugins._StarterPack.Code.Canvas.Base.Panels;
using TMPro;

public class SkinPanel : MonoBehaviour
{
    public FixedPanel fixedPanel;

    [Header("UI Items")]
    public Transform dollarContainer;
    public TextMeshProUGUI dollarTXT;

    public void Start()
    {
        fixedPanel = GetComponent<FixedPanel>();

        fixedPanel.onOpenEvents.AddListener(() => OnOpen());
        fixedPanel.onCloseEvents.AddListener(() => OnClose());
    }

    public void Update()
    {

    }

    public void OnOpen()
    {
        //When Panel is Open
    }

    public void OnClose()
    {
        //When Panel is Closed
    }

}
