﻿using System;
using Plugins._StarterPack.Code.Canvas.CustomPanels;
using TMPro;
using UnityEngine;

namespace _Code.StarterPack.Canvas.CustomPanels
{
    public class ConstPanel : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI compCoinProGui = null;

        private int _startCoin;

        public void SetStartValues(int startCoin)
        {
            _startCoin = startCoin;
            compCoinProGui.text = _startCoin + "";
        }

        public void UpdateCoinAnimation(float duration, int targetCoin)
        {
            var start = _startCoin;

            compCoinProGui.IncreaseNumber(start, targetCoin, duration);
        }

        private void UpdateCoin(int targetCoin)
        {
            compCoinProGui.text = Convert.ToString(targetCoin);
        }
    }
}