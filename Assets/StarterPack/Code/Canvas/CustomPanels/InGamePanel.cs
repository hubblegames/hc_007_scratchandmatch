﻿using Plugins._StarterPack.Code.Canvas.Components.LevelComponent;
using StarterPack._Code.Canvas.Components.LevelComponent;
using StarterPack._Code.MainLevel;
using UnityEngine;

namespace _Code.StarterPack.Canvas.CustomPanels
{
    public class InGamePanel : MonoBehaviour
    {
        // --- Managers
        private LevelManager _levelManager;

        private LevelText[] _levelTexts;

        public void Awake()
        {
            _levelManager = LevelManager.Ins;
            _levelTexts = GetComponentsInChildren<LevelText>();
        }

        public void UpdateTexts(int level)
        {
            foreach (var levelText in _levelTexts)
                levelText.SetLevel(level);
        }

        public void BTN_Level_Restart()
        {
            LevelManager.OnRestartLevel();
        }
    }
}