﻿using DG.Tweening;
using UnityEngine;

namespace StarterPack.Code.CameraControl
{
    [RequireComponent(typeof(Collider))]
    public class CameraTriggerCameraKey : MonoBehaviour
    {
        [Header("General Settings")] public float delay;
        public string triggerTag;
        [Header("Trigger Key")] public CameraKey cameraKey;

        private void OnTriggerEnter(Collider other)
        {
            if (!other.gameObject.CompareTag(triggerTag)) return;
            Invoke(nameof(CameraTrigger),delay);
        }

        private void CameraTrigger()
        {
            CameraManager.Ins.OpenCamera(cameraKey);
        }
    }
}