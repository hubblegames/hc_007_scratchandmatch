﻿using Cinemachine;
using UnityEngine;

namespace StarterPack.Code.CameraControl
{
    [RequireComponent(typeof(CinemachineVirtualCamera))]
    public class CameraObject : MonoBehaviour
    {
        public CameraKey cameraKey;
        
        [HideInInspector] public bool isActive;
        [HideInInspector] public CinemachineVirtualCamera virtualCamera;
        private CinemachineBasicMultiChannelPerlin _perlin;

        public int CamId => GetInstanceID();
        public int Priority
        {
            get => virtualCamera.Priority;
            set => virtualCamera.Priority = value;
        }

        public Transform FollowObject
        {
            get => virtualCamera.m_Follow;
            set => virtualCamera.m_Follow = value;
        }

        public void BuildCamera()
        {
            virtualCamera = GetComponent<CinemachineVirtualCamera>();
            _perlin = virtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
        }

        public void StartShaking(float amplitudeGain = 0.5f)
        {
            _perlin.m_AmplitudeGain = amplitudeGain;
        }

        public void StopShaking()
        {
            _perlin.m_AmplitudeGain = 0;
        }
    }
}