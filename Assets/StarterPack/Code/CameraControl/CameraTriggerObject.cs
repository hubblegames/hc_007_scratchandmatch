﻿using DG.Tweening;
using UnityEngine;

namespace StarterPack.Code.CameraControl
{
    [RequireComponent(typeof(Collider))]
    public class CameraTriggerObject : MonoBehaviour
    {
        [Header("General Settings")] public float delay;
        public string triggerTag;
        [Header("Trigger Camera")] public CameraObject cameraObject;

        private void OnTriggerEnter(Collider other)
        {
            if (!other.gameObject.CompareTag(triggerTag)) return;

            if (!other.gameObject.CompareTag(triggerTag)) return;
            Invoke(nameof(CameraTrigger), delay);
        }

        private void CameraTrigger()
        {
            CameraManager.Ins.OpenCamera(cameraObject.GetInstanceID());
        }
    }
}