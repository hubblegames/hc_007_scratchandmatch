﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace StarterPack.Code.CameraControl
{
    public class CameraManager : MonoBehaviour
    {
        private static CameraManager _ins;

        public static CameraManager Ins
        {
            get
            {
                if (_ins == null)
                    _ins = FindObjectOfType<CameraManager>();

                return _ins;
            }
        }

        [HideInInspector] public Camera mainCamera;
        [HideInInspector] public List<CameraObject> camDataList;
        [HideInInspector] public CameraObject activeCamera;

        private void Awake()
        {
            _ins = this;
            mainCamera = FindObjectOfType<Camera>();

            var cameraObjects = FindObjectsOfType<CameraObject>();
            if (cameraObjects.Length == 0)
            {
                Debug.LogError("There is no CameraObject in Scene");
                return;
            }

            foreach (var cameraObject in cameraObjects)
            {
                cameraObject.BuildCamera();
                camDataList.Add(cameraObject);
            }

            activeCamera = camDataList.Aggregate((i1, i2) => i1.Priority > i2.Priority ? i1 : i2);
            activeCamera.isActive = true;
        }
        public void OpenCamera(CameraKey cameraKey)
        {
            // --- No Camera Key
            if (!camDataList.Exists(c => c.cameraKey == cameraKey))
            {
                Debug.LogWarning("There is no camera key attached. " + cameraKey.ToString());
                return;
            }

            // --- Same Key
            if (cameraKey == activeCamera.cameraKey)
                return;

            // --- Close Old
            activeCamera.Priority = 0;
            // --- Open New
            activeCamera = camDataList.Find(c => c.cameraKey == cameraKey);
            activeCamera.virtualCamera.Priority = 10;
        }
        public void OpenCamera(int cameraId)
        {
            // --- No Camera Key
            if (!camDataList.Exists(c => c.CamId == cameraId))
            {
                Debug.LogWarning("There is no camera key attached. " + cameraId);
                return;
            }

            // --- Same Key
            if (cameraId == activeCamera.CamId)
                return;

            // --- Close Old
            activeCamera.Priority = 0;
            // --- Open New
            activeCamera = camDataList.Find(c => c.CamId == cameraId);
            activeCamera.virtualCamera.Priority = 10;
        }

        public void OpenCamera(CameraObject cameraObject)
        {
            OpenCamera(cameraObject.GetInstanceID());
        }
        
        public void ShakeWithDuration(float amplitudeGain = 0.5f, float duration = 0.3f)
        {
            Invoke(nameof(StopShake), duration);
            activeCamera.StartShaking(amplitudeGain);
        }

        public void StartShake(float amplitudeGain = 1)
        {
            activeCamera.StartShaking(amplitudeGain);
        }
        
        public void StopShake()
        {
            activeCamera.StopShaking();
        }
    }
}