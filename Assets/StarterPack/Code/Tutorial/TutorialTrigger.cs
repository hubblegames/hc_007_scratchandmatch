﻿using Plugins._StarterPack.Code.Canvas.CustomPanels;
using UnityEngine;

namespace Plugins._StarterPack.Code.Tutorial
{
    public class TutorialTrigger : MonoBehaviour
    {
        [Header("Values")] public TutorialAnimationType tutorialAnimationType;

        [Header("Components")] public TutorialPanel compTutorialPanel;

        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
                compTutorialPanel.OpenTutorial(tutorialAnimationType);
        }

        private void OnTriggerExit(Collider other)
        {
            if (other.CompareTag("Player"))
                compTutorialPanel.CloseTutorial(tutorialAnimationType);
        }
    }
}