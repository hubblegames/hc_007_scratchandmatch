﻿namespace StarterPack._Code.MainLevel
{
    public interface ILevelCanControl
    {
        void OnStart();

        void OnWin();

        void OnLose();
    }
}
