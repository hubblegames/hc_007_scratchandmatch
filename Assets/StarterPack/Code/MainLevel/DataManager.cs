﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace StarterPack._Code.MainLevel
{
    public class DataManager : MonoBehaviour
    {
        private static DataManager _ins;

        public static DataManager Ins
        {
            get
            {
                if (_ins == null)
                    _ins = FindObjectOfType<DataManager>();
                return _ins;
            }
        }

        private const string LevelKey = "PLevel";
        private const string CoinKey = "PCoin";
        private const string PrizePercentageKey = "PPrizePercentage";

        private const float PercentageIncrement = 20;
        public const int LevelCoin = 200;

        public static int Level
        {
            get
            {
                if (!PlayerPrefs.HasKey(LevelKey))
                    PlayerPrefs.SetInt(LevelKey, 1);

                return PlayerPrefs.GetInt(LevelKey);
            }
            set => PlayerPrefs.SetInt(LevelKey, value);
        }

        public static int Coin
        {
            get
            {
                if (!PlayerPrefs.HasKey(CoinKey))
                    PlayerPrefs.SetInt(CoinKey, 1);

                return PlayerPrefs.GetInt(CoinKey);
            }
            set => PlayerPrefs.SetInt(CoinKey, value);
        }

        public static float PrizePercentage
        {
            get
            {
                if (!PlayerPrefs.HasKey(PrizePercentageKey))
                    PlayerPrefs.SetFloat(PrizePercentageKey, 0);

                return PlayerPrefs.GetFloat(PrizePercentageKey);
            }
            set
            {
                if (value >= 100)
                    value = 0;

                PlayerPrefs.SetFloat(PrizePercentageKey, value);
            }
        }

        public static float PrizePercentageIncrease(int level)
        {
            return PercentageIncrement;
        }

        public static int MaxLevel => SceneManager.sceneCountInBuildSettings-1;
    }
}