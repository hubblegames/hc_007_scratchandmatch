﻿using _Code.StarterPack.Canvas.CustomPanels;
using Code.Scripts.Manager.Canvas.Enum;
using DG.Tweening;
using Plugins._StarterPack.Code.Canvas.Base;
using StarterPack.Code.CameraControl;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace StarterPack._Code.MainLevel
{
    public class LevelManager : MonoBehaviour
    {
        private static LevelManager _ins;

        public static LevelManager Ins
        {
            get
            {
                if (_ins == null)
                    _ins = FindObjectOfType<LevelManager>();
                return _ins;
            }
        }

        // --- Managers
        private BaseCanvasManager _baseCanvasManager;
        private CameraManager _compCameraManager;

        // --- Level
        public List<GameObject> willAdded = new List<GameObject>();
        private ILevelCanControl[] _compLevelCanStarts;

        private void Awake()
        {

            _ins = this;
            _compCameraManager = CameraManager.Ins;
            _compLevelCanStarts = willAdded.Select(s => s.GetComponent<ILevelCanControl>()).ToArray();

        }
        private void Start()
        {
            _baseCanvasManager = BaseCanvasManager.Ins;

            // --- Update Level Texts
            var inGamePanel = _baseCanvasManager.FindPanel<InGamePanel>();
            inGamePanel.UpdateTexts(DataManager.Level);

            // --- Update Win Panel
            var compWinPanel = _baseCanvasManager.FindPanel<WinPanel>();
            compWinPanel.SetStartValues(DataManager.LevelCoin, DataManager.PrizePercentage);

            // --- Update Current Coin
            var constPanel = _baseCanvasManager.FindPanel<ConstPanel>();
            constPanel.SetStartValues(DataManager.Coin);
        }
        public void OnStart()
        {

            _compCameraManager.OpenCamera(CameraKey.Camera1);
            _baseCanvasManager.OpenPanelById(CanvasId.InGame);
            foreach (var canStart in _compLevelCanStarts)
                canStart.OnStart();
        }

        public void OnLose()
        {

            DOTween.Sequence().AppendInterval(1f)
                .AppendCallback(() =>
                {
                    _baseCanvasManager.ClosePanelById(CanvasId.InGame);
                    _baseCanvasManager.OpenPanelById(CanvasId.Lose);
                });
            
            foreach (var canStart in _compLevelCanStarts)
                canStart.OnLose();
        }

        public void OnWin()
        {

            DataManager.Coin += DataManager.LevelCoin;
            DataManager.PrizePercentage += DataManager.PrizePercentageIncrease(DataManager.Level);
            DataManager.Level++;

            _baseCanvasManager.ClosePanelById(CanvasId.InGame);

            foreach (var canStart in _compLevelCanStarts)
                canStart.OnWin();

            var increase = DataManager.PrizePercentageIncrease(DataManager.Level);
            _baseCanvasManager.FindPanel<WinPanel>().OnOpen(increase, DataManager.Coin);
            _baseCanvasManager.OpenPanelById(CanvasId.Win);
        }

        public void OnPause()
        {
        }

        public static void OnRestartLevel()
        {
            DOTween.KillAll();
            DOTween.Clear();
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        public static void OnNextLevel()
        {
            DOTween.KillAll();
            DOTween.Clear();
            LevelSelector.OpenLevel(DataManager.Level);
        }
    }
}