﻿using UnityEngine.SceneManagement;

namespace StarterPack._Code.MainLevel
{
    public static class LevelSelector 
    {
        public static void OpenLevel(int level)
        {
            var maxLevel = DataManager.MaxLevel;
            
            // --- Only One Level
            if (maxLevel == 1)
            {
                SceneManager.LoadScene(1);
            }
            // --- Loop
            else if (level > maxLevel)
            {
                var val = level % maxLevel;
                if (val == 0)
                    SceneManager.LoadScene(maxLevel + 1);
                else
                    SceneManager.LoadScene(val + 1);
            }
            else
                SceneManager.LoadScene(level + 1);
        }
    }
}