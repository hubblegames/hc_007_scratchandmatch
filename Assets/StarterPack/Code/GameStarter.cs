﻿using StarterPack._Code.MainLevel;
using UnityEngine;
using Tabtale.TTPlugins;

namespace StarterPack._Code
{
    public class GameStarter : MonoBehaviour
    {
        private void Awake()
        {
            TTPCore.Setup();
        }

        private void Start()
        {
            LevelSelector.OpenLevel(DataManager.Level);
        }
    }
}