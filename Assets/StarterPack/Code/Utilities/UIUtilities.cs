﻿using DG.Tweening;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace Plugins._StarterPack.Code.Utilities
{
    public static class UiUtilities
    {
        public static Tween HorizontalMove(this RectTransform target, bool positive = true, bool tween = true,
            float duration = 0.5f, float rate = 1)
        {
            var width = target.rect.width;
            var pos = target.anchoredPosition;
            pos.x += (positive ? width : -width) * rate;

            if (tween)
                return target.DOAnchorPosX(pos.x, duration);

            target.anchoredPosition = pos;
            return null;
        }

        public static Tween VerticalMove(this RectTransform target, bool positive = true, bool tween = true,
            float duration = 0.5f, float rate = 1)
        {
            var height = target.rect.height;
            var pos = target.anchoredPosition;
            pos.y += (positive ? height : -height) * rate;

            if (tween)
                return target.DOAnchorPosY(pos.y, duration);

            target.anchoredPosition = pos;
            return null;
        }

        public static Tween ScaleLoop(this RectTransform target, Vector3? start = null, Vector3? end = null,
            float startDur = 1, float endDur = 1,
            int loopCount = -1, LoopType mode = LoopType.Yoyo, string id = "ScaleLoop")
        {
            start = start == null ? new Vector3(1, 1, 1) : start.Value;
            end = end == null ? new Vector3(1.5f, 1.5f, 1.5f) : end.Value;

            return DOTween.Sequence()
                .Append(target.DOScale(start.Value, 1f))
                .AppendCallback(() =>
                {
                    target.DOScale(end.Value, 1f)
                        .SetLoops(loopCount, mode)
                        .SetId(id);
                })
                .SetId(id);
        }

        public static Tween ScaleInitialize(this RectTransform target, Vector3? scaleTo = null,
            Vector3? scaleFrom = null, int loop = -1, float fromDur = 0.1f, float toDur = 0.1f, float loopDur = 0.5f)
        {
            scaleFrom = scaleFrom.HasValue ? scaleFrom.Value : new Vector3(1, 1, 1);
            scaleTo = scaleTo.HasValue ? scaleTo.Value : new Vector3(1.1f, 1.1f, 1.1f);

            DOTween.Kill(target);

            var seq = DOTween.Sequence()
                .Append(target.DOScale(scaleFrom.Value, fromDur).SetEase(Ease.Linear))
                .SetId(target);

            if (loop != 0)
            {
                seq.Append(
                    DOTween.Sequence()
                        .Append(target.DOScale(scaleTo.Value, loopDur))
                        .SetLoops(loop, LoopType.Yoyo)
                        .SetId(target)
                );
            }

            return seq;
        }

        public static Tween ScaleHide(this RectTransform target, Vector3? scaleTo = null, float dur = 0.1f,
            bool destroy = false)
        {
            DOTween.Kill(target);
            scaleTo = scaleTo.HasValue ? scaleTo.Value : new Vector3(0, 0, 0);

            return DOTween.Sequence()
                .Append(target.DOScale(scaleTo.Value, dur))
                .AppendCallback(() =>
                {
                    if (destroy)
                        Object.Destroy(target.gameObject);
                })
                .SetId(target);
        }
#if UNITY_EDITOR
        [MenuItem("UI/Anchor Around Object")]
        public static void AnchorAroundObject()
        {
            var o = Selection.activeGameObject;
            if (o == null || o.GetComponent<RectTransform>() == null) return;

            var r = o.GetComponent<RectTransform>();

            Undo.RecordObject(r, "Set anchors around object");
            var ratio = o.GetComponent<AspectRatioFitter>();

            var isEnable = false;
            if (ratio != null)
            {
                isEnable = ratio.enabled;
                ratio.enabled = false;
            }

            var p = o.transform.parent.GetComponent<RectTransform>();

            var offsetMin = r.offsetMin;
            var offsetMax = r.offsetMax;
            var _anchorMin = r.anchorMin;
            var _anchorMax = r.anchorMax;

            var parent_width = p.rect.width;
            var parent_height = p.rect.height;

            var anchorMin = new Vector2(_anchorMin.x + (offsetMin.x / parent_width),
                _anchorMin.y + (offsetMin.y / parent_height));
            var anchorMax = new Vector2(_anchorMax.x + (offsetMax.x / parent_width),
                _anchorMax.y + (offsetMax.y / parent_height));

            r.anchorMin = anchorMin;
            r.anchorMax = anchorMax;

            r.offsetMin = new Vector2(0, 0);
            r.offsetMax = new Vector2(0, 0);
            r.pivot = new Vector2(0.5f, 0.5f);

            if (ratio != null && isEnable)
                ratio.enabled = true;
        }
#endif
    }
}